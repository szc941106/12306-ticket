package com.easyticket.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.easyticket.util.HttpClientUtil;

/**
 * 动态秘钥
 * 
 * @author lenovo
 *
 */
public class Device {

	private static final Logger logger = Logger.getLogger(Device.class);

	public static void init() {
		JSONObject jsonObject = getDeviceId();
		if (jsonObject != null) {
			BasicClientCookie exp = new BasicClientCookie("RAIL_EXPIRATION", jsonObject.getString("exp"));
			exp.setDomain(HeaderSotre.host);
			exp.setPath("/");
			CookieStore.cookieStore.addCookie(exp);

			BasicClientCookie DEVICEID = new BasicClientCookie("RAIL_DEVICEID", jsonObject.getString("dfp"));
			DEVICEID.setDomain(HeaderSotre.host);
			DEVICEID.setPath("/");
			CookieStore.cookieStore.addCookie(DEVICEID);

		}

	}

	/**
	 * exp
	 * 
	 * @return exp dfp
	 */
	public static JSONObject getDeviceId() {
		try {
			String url = Api.getBrowserDeviceId
					+ "?algID=Bk7d81WjFa&hashCode=yQ5-jAlvHm6RccvXeliG6CCLwF5bTi-1K5KRMaZVVA0&FMQw=0&q4f3=zh-CN&VySQ=FGE8-ztrZHjoQKFkw9FzanJmGHUMxCjZ&VPIf=1&custID=133&VEek=unknown&dzuS=0&yD16=0&EOQP=eea1c671b27b7f53fb4ed098696f3560&lEnu=3232235784&jp76=d41d8cd98f00b204e9800998ecf8427e&hAqN=Win32&platform=WAP&ks0Q=d41d8cd98f00b204e9800998ecf8427e&TeRS=352x400&tOHY=24xx352x400&Fvje=i1l1o1s1&q5aJ=-8&wNLf=43b910dd8545535a83b0e957bed55965&0aew=Mozilla/5.0%20(Linux;%20Android%206.0;%20Nexus%205%20Build/MRA58N)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/63.0.3239.132%20Mobile%20Safari/537.36&wapSmartID=643c120dc3254e39b96eeb5dd8ac797b&timestamp="
					+ System.currentTimeMillis();

			HttpGet get = new HttpGet(url);
			get.addHeader("Host", HeaderSotre.host);
			get.addHeader("Referer", "https://www.12306.cn/index/");
			get.addHeader("User-Agent", HeaderSotre.userAgent);

			CloseableHttpResponse re = HttpClientUtil.getClient().execute(get);
			String reuslt = EntityUtils.toString(re.getEntity());
			return JSON.parseObject(reuslt.replaceFirst("callbackFunction", "").replaceAll("\\(", "")
					.replaceAll("\\)", "").replaceAll("'", ""));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getAlgId() {
		try {
			String url = "https://kyfw.12306.cn/otn/HttpZF/GetJS";
			HttpGet get = new HttpGet(url);
			get.addHeader("Referer", "https://www.12306.cn/index/index.html");
			get.addHeader("User-Agent", HeaderSotre.userAgent);
			CloseableHttpResponse re = HttpClientUtil.getClient().execute(get);
			String reuslt = EntityUtils.toString(re.getEntity());
			Pattern p = Pattern.compile("algID\\\\x3d(.*?)\\\\x26");
			Matcher m = p.matcher(reuslt);
			while (m.find()) {
				return m.group(1);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
